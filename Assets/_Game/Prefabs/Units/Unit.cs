﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour {

    public enum State { Pasive, Aggresive };

    public BattleController BC;
    
    [Header("Unit Config")]
    public int faction = -1;
    public State state = State.Aggresive;
    public GameObject sacrificeEffect;
    public GameObject levelUpEffect;
    public GameObject levelUpPanel;
    public GameObject spawnEffect;
    public GameObject goldUpPanel;
    public GameObject namePlatePrefab;

    [Space(5)]
    [Header("Base Stats")]
    public int STARTING_LEVEL = 0;
    public float BASE_HEALTH = 100;
    public float HEALTH_PER_LEVEL = 50;
    public float POWER = 1;
    public float SPEED = 1;
    public float VISION_RANGE = 3.0f;
    public float RANGE = 1;
    public int GOLD_REWARD;
    public string CLASS_NAME;

    [Space(5)]
    [Header("Current Stats")]
    public int level = 0;
    public float exp = 0;
    public float MAX_HEALTH;
    public float health;
    public string name = "0000";
    
    //Private values
    public Unit target;
    private bool dead = false;
    
	void Start () {
        //Connect to the BattleController
        this.BC = GameObject.Find("Main Camera").GetComponent<BattleController>();
        BC.ListUnit(this);

        //Initialize my stats
        if (this.name == "0000") {
            this.name = GenerateName();
        }
        this.level = STARTING_LEVEL;
        this.MAX_HEALTH = BASE_HEALTH;
        this.health = MAX_HEALTH;

        //Create my Nameplate
        GameObject myNameplate = (GameObject) Instantiate(namePlatePrefab,GameObject.Find("Canvas").transform);
        myNameplate.GetComponent<Nameplate>().unit = this;
        myNameplate.transform.SetAsFirstSibling();

        //Spawn VFX
        Instantiate(spawnEffect, this.transform.position, this.transform.rotation);
    }
	
	void Update () {

        if (this.GetComponent<AudioSource>()) {
            this.GetComponent<AudioSource>().volume = this.GetComponent<NavMeshAgent>().velocity.magnitude/10;
            this.GetComponent<AudioSource>().pitch = 1.7f - 0.05f * level;
        }

        //My size depends on my level
        this.transform.localScale = Vector3.Lerp(new Vector3(1, 1, 1), new Vector3(1.5f, 1.5f, 1.5f), Mathf.Min(level/10.0f, 1.0f));
        //Update animation speed parameter
        this.GetComponent<Animator>().SetFloat("speed", this.GetComponent<NavMeshAgent>().velocity.magnitude);
        
        //If I'm aggresive
        if(state == State.Aggresive || target) {
            //Always look for a target near me
            Attack(BC.GetClosestVisibleEnemy(this));
        }

        //If I have a target
        if (target) {
            //Run towards my target
            this.GetComponent<NavMeshAgent>().SetDestination(target.transform.position);
            //Looking at my target (but only horizontally)
            this.transform.rotation = Quaternion.LookRotation(Vector3.Scale(target.transform.position - this.transform.position,new Vector3(1, 0, 1)));
            //If I'm at attack range
            if (Vector3.Distance(this.transform.position, target.transform.position) <= this.RANGE) {
                //Play fighting animation
                this.GetComponent<Animator>().SetBool("fighting", true);
                //Deal damage
                target.DoDamage(POWER * Time.deltaTime);
                this.AddExperience(POWER/2 * Time.deltaTime);
            }
            //TODO - Do I need to stop fight animation if not close enough?
            else {
                this.GetComponent<Animator>().SetBool("fighting", false);
            }
        //If I don't have a target
        } else {
            //Stop fighting animation
            this.GetComponent<Animator>().SetBool("fighting", false);
        }
    }

    public void MoveTo(Vector3 destination) {
        this.GetComponent<NavMeshAgent>().SetDestination(destination);
    }

    public void Attack(Unit newTarget) {
        //If the new target isn't null
        if (newTarget) {
            //Now this is my target
            this.target = newTarget;
            //Tell it I'm attacking it
            newTarget.Defend(this);
        }
    }

    public void Defend( Unit attacker) {
        //If I don't have target
        if (!target) {
            //The one attackingme is now my target
            target = attacker;
        }
        if(this.faction == 0) {
            //All my friends help me
            foreach (Unit ally in BC.GetMyAllies(this)) {
                ally.HelpDefend(attacker);
            }
        } else {
            //Tell surrounding enemies to defend him
            foreach (Unit ally in BC.GetMyAlliesInRange(this)) {
                ally.HelpDefend(attacker);
            }
        }
    }

    public void HelpDefend(Unit attacker) {
        //If I don't have target
        if (!target) {
            //The one attacking my friend is now my target
            target = attacker;
        }
    }

    public void DoDamage(float dmg) {
        this.health -= dmg;
        print(health);
        //Am I dead?
        if (health <= 0) {
            Die();
        }
    }

    public void AddExperience(float newExp) {
        int previousLevel = this.level;
        //Add the experience
        exp += newExp;
        //Add the levels I've gone up
        level += ((int)exp) / 100;
        //Get experience remainder
        exp = exp % 100;

        //LEVELED UP!
        if (level != previousLevel) {
            //Update MAX_HEALTH
            this.MAX_HEALTH = BASE_HEALTH + HEALTH_PER_LEVEL * level;
            //Update health (give the new health)
            this.health += HEALTH_PER_LEVEL * (level - previousLevel);
            GameObject levelUpGameObject = (GameObject) Instantiate(levelUpPanel,GameObject.Find("Canvas").transform);
            levelUpGameObject.GetComponent<BubblePanel>().startPosition = GameObject.Find("Main Camera").GetComponent<Camera>().WorldToScreenPoint(this.transform.position + Vector3.up * 2);

        }
    }

    public void Die() {
        if (faction != 0 && dead == false) {
            BC.gold += this.GOLD_REWARD;
            GameObject goldUpGameObject = (GameObject)Instantiate(goldUpPanel, GameObject.Find("Canvas").transform);
            goldUpGameObject.GetComponent<BubblePanel>().text.text = "+" + this.GOLD_REWARD + " Gold";
            goldUpGameObject.GetComponent<BubblePanel>().startPosition = GameObject.Find("Main Camera").GetComponent<Camera>().WorldToScreenPoint(this.transform.position + Vector3.up*2);

        }
        dead = true;
        Instantiate(sacrificeEffect, this.transform.position, this.transform.rotation);
        BC.UnlistUnit(this);
        Destroy(this.gameObject);
    }

    public void Sacrifice() {
        if (!target) {
            List<Unit> allies = BC.GetMyAllies(this);
            if (allies.Count > 0) {
                foreach (Unit ally in allies) {
                    ally.AddExperience((100 * level + exp + 100) / allies.Count + 1);
                    Instantiate(levelUpEffect, ally.transform.position, ally.transform.rotation);
                }
                DoDamage(health);
            } else {
                Debug.Log("Cannot Sacrifice, I'm the last one!");
            }
        } else {
            Debug.Log("Cannot Sacrifice in combat");
        }
    }


    public static string GenerateName() {
        List<string> names = new List<string>();
        names.Add("Neymar");
        names.Add("Robinho");
        names.Add("Oliverio");
        names.Add("Santería");
        names.Add("Filo");
        names.Add("Bouyssou");
        names.Add("Baguette");
        names.Add("Gius");
        names.Add("Pope");
        names.Add("Decre");
        names.Add("Jugo");
        names.Add("Kaiser");
        names.Add("Orballo");
        names.Add("Zulazul");
        names.Add("Maysdf");
        names.Add("Baleful");
        names.Add("Maysdf");
        names.Add("Filo 2");
        names.Add("Bigotes");
        names.Add("Carbones");
        names.Add("Juan");
        names.Add("Wan");
        names.Add("Ludum");
        names.Add("Bill");
        names.Add("Joe");
        names.Add("Destiny");
        names.Add("Xokas");
        names.Add("Paxel");
        names.Add("Rata");
        names.Add("Euge");
        names.Add("Mike");
        names.Add("Poly");
        names.Add("Iago");
        names.Add("Viere");
        names.Add("Anton");
        names.Add("Majo");
        names.Add("Tere");
        names.Add("Bernardo");
        names.Add("Roque");
        names.Add("Guille");
        names.Add("Castelao");
        names.Add("Irwin");
        names.Add("Bill Nye");
        names.Add("Tyson");
        names.Add("De Grease");
        names.Add("McGregor");
        names.Add("Khabib");
        names.Add("Nei Palm");
        names.Add("Kaiyote");
        names.Add("Gogoplato");
        names.Add("Caliza");
        names.Add("Koi");
        names.Add("ChillHop");
        names.Add("Eddie");
        names.Add("Bravo");
        names.Add("Zurdo");
        names.Add("Little Mac");
        names.Add("Cauchemar");
        names.Add("Busy P");
        names.Add("Xplicit");
        names.Add("Neyko");
        names.Add("Gun");
        names.Add("Panceto");
        names.Add("Exlynx");
        names.Add("Petman");
        names.Add("Carro");
        names.Add("Blender");
        names.Add("Gimp");
        names.Add("Houdini");
        return names[Random.Range(0, names.Count)];
    }
}