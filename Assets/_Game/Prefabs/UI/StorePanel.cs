﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorePanel : MonoBehaviour {

    public UnitPlate client;
    public List<OfferPlate> offerPlates;
    private BattleController BC;

    private Vector2 targetPosition;

    void Start() {
        BC = GameObject.Find("Main Camera").GetComponent<BattleController>();
        //Ocultar panel
        targetPosition = new Vector2(-17.5f, 0);
    }

    // Update is called once per frame
    void Update() {
        if (client) {

            //Actualizar ofertas
            for(int i = 0; i < offerPlates.Count; i++) {

                switch (BC.offers[i].unit.CLASS_NAME) {
                    case "Melee":
                        offerPlates[i].Background.color = new Color(1, 0.7f, 0.45f);
                        break;
                    case "Ranged":
                        offerPlates[i].Background.color = new Color(0.69f, 0.66f, 1);
                        break;
                    case "Tank":
                        offerPlates[i].Background.color = new Color(1, 0.5f, 0.65f);
                        break;
                    default:
                        break;
                }
                offerPlates[i].nameText.text = BC.offers[i].unit.name;
                offerPlates[i].levelText.text = BC.offers[i].unit.level.ToString();
                offerPlates[i].healthText.text = (BC.offers[i].unit.MAX_HEALTH + BC.offers[i].unit.HEALTH_PER_LEVEL* BC.offers[i].unit.level).ToString();
                offerPlates[i].powerText.text = BC.offers[i].unit.POWER.ToString();
                offerPlates[i].classText.text = BC.offers[i].unit.CLASS_NAME;
                //Activate/Deactivate button depending on the gold the player has
                if (BC.gold >= BC.offers[i].cost) {
                    offerPlates[i].BuyButton.interactable = true;
                    offerPlates[i].costText.text = "Buy: " + BC.offers[i].cost.ToString() + " gold";
                } else {
                    offerPlates[i].BuyButton.interactable = false;
                    offerPlates[i].costText.text = "Need " + BC.offers[i].cost.ToString() + " gold";
                }
            }

            //Mostrar panel
            targetPosition = new Vector2(-17.5f, 115);
        } else {
            //Ocultar panel
            targetPosition = new Vector2(-17.5f, -20);
        }
        
        this.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(GetComponent<RectTransform>().anchoredPosition, targetPosition, Time.deltaTime*10.0f);

    }

    public void Buy(int offerId) {
        Offer selectedOffer = BC.offers[offerId];
        BC.offers.Remove(selectedOffer);
        //Seleccionamos la unidad que queremos comprar
        Unit newUnit = selectedOffer.unit;
        //Colocamos la nueva unidad en una posición cerca de algun aliado
        List<Unit> allies = BC.GetAllies();
        newUnit.transform.position = allies[0].transform.position + Vector3.forward*2;
        //Activamos la nueva unidad
        newUnit.gameObject.SetActive(true);
        //Vinculamos la unidad al UnitPlate cliente
        client.myUnit = newUnit;
        //Restamos el oro
        BC.gold -= selectedOffer.cost;
        //Cancelamos las demas ofertas
        BC.offers.Clear();
        //Cerramos la tienda
        Close();
    }

    public void Open(UnitPlate client) {
        //Habilitar hijos
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild(i).gameObject.SetActive(true);
        }
        //Esperar a la respuesta del jugador
        this.client = client;
    }
    public void Close() {
        //Deshabilitar hijos
        for(int i = 0; i< 3; i++) {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        //Esperar a la respuesta del jugador
        this.client = null;
    }
}
