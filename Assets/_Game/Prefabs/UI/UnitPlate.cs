﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitPlate : MonoBehaviour {

    public Unit myUnit;
    public GameObject activeUnitPanel;
    public ProgressBar healthBar;
    public ProgressBar expBar;
    public Text levelText;
    public Text nameText;
    public Button sacrificeButton;

    private StorePanel store;

    private void Start() {
        store = GameObject.Find("Store").GetComponent<StorePanel>();
    }

    void Update () {

        //Only enabled if my unit exists
        activeUnitPanel.SetActive(myUnit);

        //Update UI
        if (healthBar) {
            healthBar.value = myUnit.health/myUnit.MAX_HEALTH;
        }
        if (expBar) {
            expBar.value = ((float)myUnit.exp) / 100.0f;
        }
        if (levelText) {
            levelText.text = myUnit.level.ToString();
        }
        if (nameText) {
            nameText.text = myUnit.name;
        }

        //Disable sacrifice button in combat
        sacrificeButton.interactable = (myUnit.target == null);
    }

    //TODO - Debugging purposes
    public void ExperienceButton() {
        myUnit.AddExperience(30);
    }

    public void SacrificeButton() {
        if(myUnit.target == null){
            myUnit.Sacrifice();
        }
    }

    public void OpenStoreButton() {
        store.Open(this);
    }

    public void CloseStoreButton() {
        store.Close();
    }
}
