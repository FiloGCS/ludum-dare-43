﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfferPlate : MonoBehaviour {
    
    public Text nameText;
    public Text levelText;
    public Text costText;
    public Text classText;
    public Text healthText;
    public Text powerText;
    public Image Background;
    public Button BuyButton;
    
}
