﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offer {
    public Unit unit;
    public int cost;

    public Offer(Unit unit, int cost) {
        this.unit = unit;
        this.cost = cost;
    }
}
