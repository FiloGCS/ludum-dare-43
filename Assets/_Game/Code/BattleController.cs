﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleController : MonoBehaviour {


    public List<GameObject> AllyPrefab;
    public GameObject EnemyPrefab;
    public GameObject clicEffect;

    public WinPanel winPanel;
    public LosePanel losePanel;
    

    public int gold = 0;

    public List<Offer> offers;
    private List<Unit> units;
    private Text goldText;

    //For win and lose testing
    private Collider finishArea;
    private Transform centerMarker;

    private void Awake() {
        units = new List<Unit>();
        offers = new List<Offer>();
        //Fill offers
        for (int i = offers.Count; i < 3; i++) {
            offers.Add(GenerateOffer());
        }
    }

    private void Start() {
        goldText = GameObject.Find("Gold Text").GetComponent<Text>();
        finishArea = GameObject.Find("Finish").GetComponent<BoxCollider>();
        centerMarker = GameObject.Find("CenterMarker").transform;
    }

    private void Update() {
        

        //Check for win
        if (finishArea.bounds.Contains(centerMarker.position)) {
            print("WIN");
            winPanel.gameObject.SetActive(true);
        }

        //Update UI
        goldText.text = gold.ToString();

        //Refill offers
        for(int i = offers.Count; i<3; i++) {
            offers.Add(GenerateOffer());
        }

        //Right clic to move all ally troops
        if (Input.GetMouseButton(1)) {
            RaycastHit hit;
            Ray ray = this.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            int layers = LayerMask.GetMask("Terrain");
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers)) {
                foreach (Unit ally in GetAllies()) {
                    ally.MoveTo(hit.point);
                    //TODO - Instantiate a clic effect
                    Instantiate(clicEffect, hit.point,Quaternion.Euler(Vector3.forward));

                }
            }
        }

    }

    public void UnlistUnit(Unit unit) {
        units.Remove(unit);
        List<Unit> allies = GetAllies();
        if(allies.Count <= 0) {
            print("LOSE");
            losePanel.gameObject.SetActive(true);
        }
    }
    public void ListUnit(Unit unit) {
        units.Add(unit);
    }

    public Unit GetClosestEnemy(Unit me) {
        Unit closestEnemy = null;
        foreach (Unit unit in units) {
            if (me.faction != unit.faction
            && unit.gameObject.activeInHierarchy) {
                if(!closestEnemy || Vector3.Distance(me.transform.position,unit.transform.position) < Vector3.Distance(me.transform.position, closestEnemy.transform.position)) {
                    closestEnemy = unit;
                }                   
            }
        }
        return closestEnemy;
    }

    public Unit GetClosestVisibleEnemy(Unit me) {
        Unit closest = null;
        //For each unit
        foreach (Unit unit in units) {
            //If he's enemy and I can see him
            if (me.faction != unit.faction
            && me.VISION_RANGE >= Vector3.Distance(me.transform.position,unit.transform.position)
            && unit.gameObject.activeInHierarchy) {
                //If he's closer than the closest one
                if (!closest || Vector3.Distance(me.transform.position, unit.transform.position) < Vector3.Distance(me.transform.position, closest.transform.position)) {
                    closest = unit;
                }
            }
        }
        return closest;
    }
    public Unit GetClosestFromGroup(Unit me, List<Unit> group) {
        Unit closest = null;
        foreach (Unit unit in group) {
            if (!closest
            || Vector3.Distance(me.transform.position, unit.transform.position) < Vector3.Distance(me.transform.position, closest.transform.position)
            && unit.gameObject.activeInHierarchy) {
                closest = unit;
            }
        }
        return closest;
    }

    public List<Unit> GetUnitsFromFaction(int faction) {
        List<Unit> allies = new List<Unit>();
        foreach (Unit unit in units) {
            if(unit.faction == faction
            && unit.gameObject.activeInHierarchy) {
                allies.Add(unit);
            }
        }
        return allies;
    }
    public List<Unit> GetEnemiesFromFaction(int faction) {
        List<Unit> allies = new List<Unit>();
        foreach (Unit unit in units) {
            if (unit.faction != faction
            && unit.gameObject.activeInHierarchy) {
                allies.Add(unit);
            }
        }
        return allies;
    }
    public List<Unit> GetMyAllies(Unit me) {
        List<Unit> allies = new List<Unit>();
        foreach (Unit unit in units) {
            if (unit.faction==me.faction
            && me!=unit
            && unit.gameObject.activeInHierarchy) {
                allies.Add(unit);
            }
        }
        return allies;
    }
    public List<Unit> GetMyAlliesInRange(Unit me) {
        List<Unit> allies = new List<Unit>();
        foreach (Unit unit in units) {
            if (unit.faction == me.faction
            && me != unit
            && unit.gameObject.activeInHierarchy
            && Vector3.Distance(me.transform.position,unit.transform.position) <= unit.VISION_RANGE) {
                allies.Add(unit);
            }
        }
        return allies;
    }
    public List<Unit> GetAllies() {
        return GetUnitsFromFaction(0);
    }
    public List<Unit> GetEnemies() {
        return GetEnemiesFromFaction(0);
    }

    public Offer GenerateOffer() {
        //Creamos una nueva unidad a boleo entre las disponibles
        Unit newUnit = ((GameObject)Instantiate(AllyPrefab[Random.Range(0,AllyPrefab.Count)],new Vector3(999,999,999),Quaternion.Euler(Vector3.forward))).GetComponent<Unit>();
        //Randomizamos stats
        newUnit.STARTING_LEVEL = Random.Range(0, 4);
        newUnit.name = Unit.GenerateName();
        switch (newUnit.CLASS_NAME) {
            case "Melee":
                newUnit.POWER = Random.Range(7.5f, 12.5f);
                break;
            case "Ranged":
                newUnit.POWER = Random.Range(7.5f, 12.5f);
                break;
            case "Tank":
                newUnit.POWER = Random.Range(2.5f, 7f);
                break;
            default:
                newUnit.POWER = 0.0f;
                break;
        }
        int cost = (int)(newUnit.level*3 + newUnit.POWER*2) + Random.Range(0, 10);

        //La dejamos preparadita con los stats y tal
        newUnit.level = newUnit.STARTING_LEVEL;
        newUnit.MAX_HEALTH = newUnit.BASE_HEALTH + newUnit.HEALTH_PER_LEVEL * newUnit.level;
        newUnit.health = newUnit.MAX_HEALTH;

        newUnit.gameObject.SetActive(false);
        return new Offer(newUnit, cost);
    }

}
