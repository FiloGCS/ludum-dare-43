﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //Parameters
    public float ZOOM_SENSIBILITY = 0.3f;
    public float LERP_SPEED = 10;

    public float zoom = 1;
    private Vector3 cameraOffset;
    private Vector3 targetPosition;
    private Quaternion targetRotation;

    private Vector3 initialRotation;

    //Start
    void Start() {
        cameraOffset = this.transform.localPosition;
        targetPosition = this.transform.localPosition;
        initialRotation = this.transform.localRotation.eulerAngles;
    }

    //Update
    void Update() {

        //Zoom calculation
        zoom -= Input.GetAxis("Mouse ScrollWheel") * ZOOM_SENSIBILITY;
        if (zoom < 0.5f) {
            zoom = 0.5f;
        } else if (zoom > 1.0f) {
            zoom = 1.0f;
        }
        targetRotation = Quaternion.Euler(initialRotation - new Vector3((1 - zoom) * 30.0f, 0, 0));

        //Target recalculation
        targetPosition = cameraOffset * zoom;

        //Recolocation to target
        this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, targetPosition, Time.deltaTime * LERP_SPEED);
        this.transform.localRotation = Quaternion.Lerp(this.transform.localRotation, targetRotation, Time.deltaTime * LERP_SPEED);

    }
}
