﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterMarker : MonoBehaviour {

    private BattleController BC;

	// Use this for initialization
	void Start () {
        BC = GameObject.Find("Main Camera").GetComponent<BattleController>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 newPos = Vector3.zero;
        List<Unit> allies = BC.GetAllies();
        foreach (Unit ally in allies) {
            newPos += ally.transform.position;
        }
        if(allies.Count >= 1) {
            this.transform.position = newPos / allies.Count;
        }
    }
}
