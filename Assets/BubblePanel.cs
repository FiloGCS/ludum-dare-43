﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubblePanel : MonoBehaviour {

    private float startTime;
    public Vector3 startPosition;
    public Text text;
    public Image image;

    private Color startTextColor;
    private Color startImageColor;
    private Color targetTextColor;
    private Color targetImageColor;

    void Start () {
        startTime = Time.time;
        startTextColor = text.color;
        startImageColor = image.color;
        targetTextColor = text.color;
        targetTextColor.a = 0;
        targetImageColor = image.color;
        targetImageColor.a = 0;
    }
	
	void Update () {
        this.transform.position = Vector3.Lerp(startPosition, startPosition + Vector3.up * 50,(Time.time-startTime));
        this.image.color = Color.Lerp(startImageColor, targetImageColor, (Time.time - startTime));
        this.text.color = Color.Lerp(startTextColor, targetTextColor, (Time.time - startTime));

        if (Time.time-startTime >= 1) {
            Destroy(this.gameObject);
        }
	}
}
