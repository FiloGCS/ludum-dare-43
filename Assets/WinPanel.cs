﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinPanel : MonoBehaviour {


    public Text endText;
    public Button nextButton;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayNext() {
        endText.text = "Nah, just kidding.";
        Destroy(nextButton.gameObject);
    }

    public void GoToMenu() {
        SceneManager.LoadScene("Menu Scene");
    }
}
