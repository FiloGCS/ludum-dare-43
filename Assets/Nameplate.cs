﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nameplate : MonoBehaviour {

    public Unit unit;
    public ProgressBar bar;
    public Text nameText; 

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (unit) {
            if(unit.faction == 0) {
                bar.color = Color.green;
            } else {
                bar.color = Color.red;
            }
            bar.value = unit.health / unit.MAX_HEALTH;
            nameText.text = unit.name;
            this.transform.position = GameObject.Find("Main Camera").GetComponent<Camera>().WorldToScreenPoint(unit.transform.position);
        } else {
            this.transform.position = new Vector3(-999, -999, -999);
        }
	}
}
