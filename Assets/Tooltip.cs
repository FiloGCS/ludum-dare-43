﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

    public Unit unit;

    public Text nameText;
    public Text healthText;
    public Text levelText;
    public Text powerText;
    public Text classText;
    public Image background;

    private Camera camera;


    private void Start() {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    void Update () {


        //Raycast a las unidades?
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        int layers = LayerMask.GetMask("Unit_ally");
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers)) {
            Unit thisUnit = hit.transform.GetComponent<Unit>();
            if (thisUnit) {
                unit = thisUnit;
                this.transform.position = camera.WorldToScreenPoint(hit.point);
            }
        } else {
            unit = null;
            this.transform.position = new Vector3(-999, -999, -999);
        }
        
        if (unit != null) {
            this.nameText.text = unit.name;
            this.healthText.text = unit.health.ToString();
            this.levelText.text = unit.level.ToString();
            this.powerText.text = unit.POWER.ToString();
            this.classText.text = unit.CLASS_NAME;

            switch (unit.CLASS_NAME) {
                case "Melee":
                    background.color = new Color(1, 0.7f, 0.45f);
                    break;
                case "Ranged":
                    background.color = new Color(0.69f, 0.66f, 1);
                    break;
                case "Tank":
                    background.color = new Color(1, 0.5f, 0.65f);
                    break;
                default:
                    break;
            }
        }
	}
}
