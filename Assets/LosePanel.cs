﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LosePanel : MonoBehaviour {


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToMenu() {
        SceneManager.LoadScene("Menu Scene");
    }

    public void Retry() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    } 
}
